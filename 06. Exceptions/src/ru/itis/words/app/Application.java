package ru.itis.words.app;

import ru.itis.words.repositories.WordsRepository;
import ru.itis.words.repositories.WordsRepositoryFileImpl;
import ru.itis.words.services.WordsService;
import ru.itis.words.services.WordsServiceImpl;

public class Application {
    public static void main(String[] args) {
        WordsRepository wordsRepository = new WordsRepositoryFileImpl("content_words.txt");
        WordsService wordsService = new WordsServiceImpl(wordsRepository);

        System.out.println(wordsService.contains("are"));
    }
}
