package ru.itis;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import ru.itis.repositories.StudentsRepository;
import ru.itis.repositories.StudentsRepositoryJdbcTemplateImpl;

import java.io.IOException;
import java.util.Properties;

public class Main {


    public static void main(String[] args) {
        Properties properties = new Properties();
        try {
            properties.load(Main.class.getResourceAsStream("/db.properties"));
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }

        HikariConfig config = new HikariConfig();
        config.setJdbcUrl(properties.getProperty("db.url"));
        config.setUsername(properties.getProperty("db.username"));
        config.setPassword(properties.getProperty("db.password"));
        config.setMaximumPoolSize(20);

        HikariDataSource dataSource = new HikariDataSource(config);

        StudentsRepository studentsRepository = new StudentsRepositoryJdbcTemplateImpl(dataSource);
        System.out.println(studentsRepository.findAllByAgeInRangeOrderByIdDesc(21, 28));

//        ExecutorService executorService = Executors.newFixedThreadPool(50);
//
//        for (int i = 0; i < 10_000; i++) {
//            executorService.submit(() -> {
//              try {
//                  studentsRepository.save(Student.builder()
//                          .firstName("Marsel")
//                          .lastName("Sidikov")
//                          .build());
//              } catch (Exception e) {
//                  e.printStackTrace();
//                  System.exit(0);
//              }
//            });
//        }

    }
}
