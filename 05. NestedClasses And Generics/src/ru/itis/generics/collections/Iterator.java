package ru.itis.generics.collections;

public interface Iterator<C> {
    boolean hasNext();

    C next();
}
