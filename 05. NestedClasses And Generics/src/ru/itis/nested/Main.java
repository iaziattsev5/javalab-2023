package ru.itis.nested;

import ru.itis.nested.collections.Iterator;
import ru.itis.nested.collections.LinkedList;


public class Main {
    public static void main(String[] args) {
        LinkedList linkedList = new LinkedList();
        linkedList.add(5);
        linkedList.add(10);
        linkedList.add(15);

//        LinkedListIterator linkedListIterator = new LinkedListIterator(linkedList);

        Iterator iterator = linkedList.iterator();

        while (iterator.hasNext()) {
            System.out.println(iterator.next());
        }

//        LinkedList.Node node = new LinkedList.Node(10);

        LinkedList.LinkedListIterator listIterator = linkedList.new LinkedListIterator();

    }
}