package framework;

public class Bill {

    @DefaultValue(value = "12345")
    private int subscriberNo;

    @DefaultValue(value = "86.7")
    private double sum;

    @Override
    public String toString() {
        return "Bill{" +
                "subscriberNo=" + subscriberNo +
                ", sum=" + sum +
                '}';
    }
}
