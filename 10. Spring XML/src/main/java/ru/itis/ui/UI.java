package ru.itis.ui;

import ru.itis.services.UserService;
import ru.itis.services.impl.UsersServiceImpl;

import java.util.Scanner;

public class UI {

    private final Scanner scanner = new Scanner(System.in);

    private final UserService usersService;

    public UI(UserService usersService) {
        this.usersService = usersService;
    }

    public void start() {
        while (true) {
            printMainMenu();

            String command = scanner.nextLine();

            switch (command) {
                case "1" -> {
                    String email = scanner.nextLine();
                    String password = scanner.nextLine();

                    if (this.usersService.signUp(email, password)) {
                        System.out.println("Пользователь зарегистрирован");
                    } else {
                        System.out.println("Проблемы с регистрацией пользователя");
                    }
                }
                case "2" -> System.exit(0);
                default -> System.out.println("Команда не распознана");
            }
        }


    }

    private void printMainMenu() {
        System.out.println("Выберите действие:");
        System.out.println("1. Регистрация пользователя");
        System.out.println("2. Выход");
    }
}
