package ru.itis.validation.impl;

import ru.itis.exceptions.PasswordValidationException;
import ru.itis.validation.PasswordValidator;

public class PasswordByLengthValidator implements PasswordValidator {

    private final int minLength;

    public PasswordByLengthValidator(int minLength) {
        this.minLength = minLength;
    }

    public void validate(String password) throws PasswordValidationException {
        if (password.length() < minLength) {
            throw new PasswordValidationException("Short password");
        }
    }
}
