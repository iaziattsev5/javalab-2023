package ru.itis.validation.impl;

import ru.itis.exceptions.PasswordValidationException;
import ru.itis.validation.PasswordValidator;

public class PasswordByCharactersValidator implements PasswordValidator {
    private String specialCharacters;

    public void validate(String password) throws PasswordValidationException {
        int count = 0;

        for (char character : specialCharacters.toCharArray()) {
            if (password.indexOf(character) != -1) {
                count++;
            }
        }

        if (count == 0) {
            throw new PasswordValidationException("Missing special characters");
        }
    }

    public void setSpecialCharacters(String specialCharacters) {
        this.specialCharacters = specialCharacters;
    }
}
