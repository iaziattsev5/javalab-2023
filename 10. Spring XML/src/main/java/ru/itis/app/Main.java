package ru.itis.app;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import ru.itis.ui.UI;
import ru.itis.validation.PasswordValidator;

public class Main {
    public static void main(String[] args) {

        ApplicationContext context = new ClassPathXmlApplicationContext("context.xml");

        PasswordValidator passwordValidator1 = context.getBean(PasswordValidator.class);
        PasswordValidator passwordValidator2 = context.getBean(PasswordValidator.class);

        System.out.println(passwordValidator1 == passwordValidator2);
    }
}
